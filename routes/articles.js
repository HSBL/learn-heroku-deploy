var express = require("express");
var router = express.Router();
const { Article } = require("../models");

/* GET users listing. */
router.get("/", (req, res) => {
  Article.findAll().then((articles) => {
    res.json(articles);
  });
});

module.exports = router;
