var express = require("express");
var router = express.Router();

/* GET users listing. */
router.get("/", function (req, res, next) {
  res.json({ member: ["Has", "Ershad", "Arief", "Septian", "Anton", "Andy", "Kas Pul", "Akbar", "Irvandri"] });
});

module.exports = router;
