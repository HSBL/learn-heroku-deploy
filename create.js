const { Article } = require("./models");

Article.create({
  title: "Article 2",
  body: "Ipsum Dolor Sit Amet",
  approved: true,
}).then((article) => {
  console.log(article);
});
